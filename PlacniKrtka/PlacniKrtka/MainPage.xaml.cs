﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;

namespace PlacniKrtka
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        public int Trefa { get; set; } = 0;
        public int Vedle { get; set; } = 0;

        int CoinPenize = 0;
        float koeficientZmenyRychlosti = 0.92f;
        List<Krtek> frontaKrtku = new List<Krtek>();
        int pocitadloId = 0;
        int odstanitPocitadlo = 0;
        int obtiznost = 0;
        Random random = new Random();
        bool znova = false;
        int PocatecniRychlostHry = 800; //v ms
        //zrychleni po poctu kliku v ms
        int[] RychlostHry = new int[50];
        //pocet nutnych kliku ke zrychleni
        int[] PocetKliku = new int[50];
        int PocetPrvnichKliku = 10;
        int DobaTrvaniSkillu = 10000; //v ms 10sekund
        //sila placnuti
        int utok = 1;
        int CenaSkilluSila = 100; // v coinech


        public MainPage()
        {
            NactiPromenneHry();
            InitializeComponent();
            TrefaLabel.Text = Trefa.ToString();
            VedleLabel.Text = Vedle.ToString();
            CoinsLabel.Text = CoinPenize.ToString();
            //zapne casovac        
            Hrej(RychlostHry[obtiznost], PocetKliku[obtiznost]);
        }
        private void NactiPromenneHry()
        {
            RychlostHry[0] = PocatecniRychlostHry;
            PocetKliku[0] = PocetPrvnichKliku;
            for (int i = 1; i < 50; i++)
            {
                RychlostHry[i] = (int)Math.Floor(RychlostHry[i - 1] * koeficientZmenyRychlosti);
                PocetKliku[i] = PocetKliku[i - 1] + (15 * i);
            }
        }
        private void Hrej(int rychlost, int limit)
        {

            Device.StartTimer(new TimeSpan(0, 0, 0, 0, rychlost), () =>
            {
                if (Vedle == 10)
                {
                    return false;
                }

                if (odstanitPocitadlo != 0)
                {
                    UberZivot(false, odstanitPocitadlo - 1);
                }
                RozsvitButton();

                odstanitPocitadlo++;

                if (random.Next(10) == 5)
                {
                    RozsvitButton();

                }

                if (pocitadloId < limit)
                {
                    return true;
                }
                else
                {

                    obtiznost++;
                    Hrej(RychlostHry[obtiznost], PocetKliku[obtiznost]);
                    return false;
                }

            });
        }

        private void RozsvitButton()
        {
            Krtek krtek;
            do
            {
                krtek = new Krtek(pocitadloId, Krtek_Clicked, obtiznost);
                foreach (Krtek k in frontaKrtku)
                {
                    if (krtek.PozX == k.PozX && krtek.PozY == k.PozY)
                    {
                        znova = true;
                    }
                    else
                    {
                        znova = false;
                    }
                }
            } while (znova);

            pocitadloId++;
            frontaKrtku.Add(krtek);
            Grid.SetColumn(krtek.imageButton, krtek.PozX);
            Grid.SetRow(krtek.imageButton, krtek.PozY);
            //vykresleni
            ZakladGrid.Children.Add(krtek.imageButton);
        }



        //predam id
        private void UberZivot(bool sam, int id)
        {
            try
            {

                //vybrani krtka z fronty podle predaneho id
                Krtek krtecek = frontaKrtku.Where(h => h.ID.Equals(id)).First();


                //pokud se neco nacetlo do krtecka
                if (krtecek != null)
                {

                    if (!sam)
                    {
                        krtecek.Zivot = 0;
                    }
                    //ubere krteckovy zivot
                    krtecek.UberZivot(utok);

                    //kdyz ma krtecek zivot min nez 0
                    if (krtecek.Zivot <= 0)
                    {
                        frontaKrtku.Remove(krtecek);
                        ZakladGrid.Children.Remove(krtecek.imageButton);


                        //kdyz bude odpocet
                        if (!sam)
                        {
                            //ale muzu ho zabit tak ubere
                            if (krtecek.MuzuZabit)
                            {
                                Xamarin.Essentials.Vibration.Vibrate(50);
                                Vedle++;
                                VedleLabel.Text = Vedle.ToString();
                            }
                        }

                        //kdy zabiju sam
                        else
                        {
                            // ale nemuzu zabit 
                            if (!krtecek.MuzuZabit)
                            {
                                Xamarin.Essentials.Vibration.Vibrate(50);
                                Vedle++;
                                VedleLabel.Text = Vedle.ToString();
                            }
                            else
                            {
                                Trefa++;
                                TrefaLabel.Text = Trefa.ToString();
                                CoinPenize += krtecek.Bounty;
                                CoinsLabel.Text = CoinPenize.ToString();
                            }
                        }
                    }
                }
                if (Vedle == 10)
                {

                    DisplayAlert("Prohra", string.Format("Tvé skóre je {0}", Trefa), "ok");
                    Navigation.PopAsync();
                    Navigation.PushAsync(new HighScore(true, Trefa));

                }

            }

            //zachytavaní chyb
            catch
            {

            }
        }



        private void Krtek_Clicked(object sender, EventArgs e)
        {
            UberZivot(true, int.Parse(((ImageButton)sender).ClassId));

        }



        async void SkillSilaButton_Clicked(object sender, EventArgs e)
        {

            if (CoinPenize >= CenaSkilluSila)
            {
                CoinPenize -= CenaSkilluSila;
                utok = 2;
                Device.StartTimer(TimeSpan.FromMilliseconds(DobaTrvaniSkillu), () =>
                {
                    utok = 1;
                    return false;
                });
            }
        }
    }
}
