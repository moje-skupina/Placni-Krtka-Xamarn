﻿using SQLite;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlacniKrtka
{
    public class Database
    {
        readonly SQLiteAsyncConnection databaze;

        public Database(string dbPath)
        {
            databaze = new SQLiteAsyncConnection(dbPath);
            databaze.CreateTableAsync<OsobaSkore>().Wait();
        }

        public Task<List<OsobaSkore>> GetPeopleAsync()
        {
            return databaze.Table<OsobaSkore>().ToListAsync();
        }
        public Task<int> SavePersonAsync(OsobaSkore osoba)
        {
            return databaze.InsertAsync(osoba);
        }
    }
}
